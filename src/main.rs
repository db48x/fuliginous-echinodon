#![allow(non_snake_case)]
use std::convert::TryInto;
use std::collections::HashMap;
use std::f64::consts::TAU;

use bevy::prelude::*;
use bevy::core::FixedTimestep;
use bevy::math::*;
use bevy::diagnostic::{FrameTimeDiagnosticsPlugin, LogDiagnosticsPlugin};

use bevy_egui::{egui, EguiContext, EguiPlugin};
use bevy_inspector_egui::{Inspectable, InspectableRegistry, WorldInspectorPlugin};
use bevy_prototype_lyon::prelude::*;

#[derive(Inspectable, Default)]
struct Dot {
    creation: f64,
    color: Color
}
#[derive(Inspectable, Default)]
struct Star;
#[derive(Inspectable, Default)]
struct Planet;
#[derive(Inspectable, Default)]
struct JumpPoint;
#[derive(Inspectable, Default)]
struct Fleet;
#[derive(Inspectable, Default)]
struct SmartFleet;
#[derive(Inspectable, Clone, Debug, Default)]
struct Orbit {
    radius: f32,
    speed: f32,
    true_anomaly: f32,
}
#[derive(Inspectable, Clone, Debug)]
struct Flight {
    target: Entity,
    v: f32,
    dp: Option<Vec3>,
}
struct SimpleReckoning;
struct SmarterReckoning;
struct GridSearch;
struct GradientDescent;

struct Trails {
    enabled: bool,
    length: f64,
}

#[derive(Default)]
struct ShipTypeSpawns {
    simple: bool,
    smarter: bool,
    grid: bool,
    gradient: bool,
}

#[derive(Default)]
struct ShipSpawnCounter {
    simple: usize,
    smarter: usize,
    grid: usize,
    gradient: usize,
}

//#[derive(Default)]
struct ThePlanet(Entity);

fn main() {
    let mut builder = App::build();
    let mut registry = builder
        .world_mut()
        .get_resource_or_insert_with(InspectableRegistry::default);
    registry.register::<Dot>();
    registry.register::<Star>();
    registry.register::<Planet>();
    registry.register::<JumpPoint>();
    registry.register::<Fleet>();
    registry.register::<Orbit>();
    registry.register::<Flight>();

    builder
        .insert_resource(Msaa { samples: 8 })
        .insert_resource(ClearColor(Color::MIDNIGHT_BLUE))
        .insert_resource(ShipTypeSpawns { simple: false, smarter: false, grid: false, gradient: false })
        .insert_resource(ShipSpawnCounter { simple: 0, smarter: 0, grid: 0, gradient: 0 })
        .insert_resource(Trails { enabled: false, length: 1.0 })
        .add_plugins(DefaultPlugins)
        //.add_plugin(LogDiagnosticsPlugin::default())
        .add_plugin(FrameTimeDiagnosticsPlugin::default())
        .add_plugin(EguiPlugin)
        .add_system(some_ui.system())
        .add_plugin(WorldInspectorPlugin::new())
        .add_plugin(ShapePlugin)
        .add_system(dot_decay.system())
        .add_system(orbit_system.system())
        .add_system(ship_get_target.system().chain(simple_ship_system.system()))
        .add_system(ship_get_target.system().chain(smarter_ship_system.system()))
        .add_system(ship_get_target.system().chain(grid_ship_set_course.system()))
        .add_system(ship_get_target.system().chain(gradient_ship_set_course.system()))
        .add_system_set(
            SystemSet::new()
                .with_run_criteria(FixedTimestep::steps_per_second(1.0))
                .with_system(jp_spawn_system.system()))
        .add_startup_system(setup.system())
        .run();
}

fn setup(mut commands: Commands) {
    commands.spawn_bundle(OrthographicCameraBundle::new_2d());
    commands.spawn_bundle(UiCameraBundle::default());

    let star = commands.spawn()
        .insert(Star)
        .insert(Transform::from_xyz(0.0, 0.0, 0.0))
        .insert_bundle(GeometryBuilder::build_as(
            &shapes::Circle {
                radius: 10.0,
                center: Vec2::ZERO,
            },
            ShapeColors::new(Color::GOLD),
            DrawMode::Fill(FillOptions::DEFAULT),
            Transform::default(),
        ))
        .id();

    let planet = commands.spawn()
        .insert(Planet)
        .insert(Orbit { radius: 300.0, speed: 0.2, true_anomaly: 1.23 })
        .insert_bundle(GeometryBuilder::build_as(
            &shapes::Circle {
                radius: 3.0,
                center: Vec2::ZERO,
            },
            ShapeColors::new(Color::CYAN),
            DrawMode::Fill(FillOptions::DEFAULT),
            Transform::default(),
        ))
        .id();
    commands.insert_resource(ThePlanet(planet));

    let moon = commands.spawn()
        .insert(Planet)
        .insert(Orbit { radius: 10.0, speed: 1.4, true_anomaly: 3.141592 })
        .insert_bundle(GeometryBuilder::build_as(
            &shapes::Circle {
                radius: 1.0,
                center: Vec2::ZERO,
            },
            ShapeColors::new(Color::CYAN),
            DrawMode::Fill(FillOptions::DEFAULT),
            Transform::default(),
        ))
        .id();

    commands.entity(star).push_children(&[planet]);
    commands.entity(planet).push_children(&[moon]);

    commands.spawn()
        .insert(JumpPoint)
        .insert_bundle(GeometryBuilder::build_as(
            &shapes::Circle {
                radius: 2.0,
                center: Vec2::ZERO,
            },
            ShapeColors::new(Color::ORANGE),
            DrawMode::Fill(FillOptions::DEFAULT),
            Transform::from_xyz(600.0, 300.0, 0.0),
        ));
}

fn some_ui(egui_context: Res<EguiContext>,
           mut trails: ResMut<Trails>,
           mut spawning: ResMut<ShipTypeSpawns>) {
    egui::Window::new("trails").anchor(egui::Align2::LEFT_BOTTOM, (16.0, -16.0))
        .show(egui_context.ctx(),
              |ui| {
                  ui.checkbox(&mut trails.enabled, "Enabled");
                  ui.add(egui::Slider::new(&mut trails.length, 0.1..=5.0).text("Length"));
              });
    egui::Window::new("ship spawner").anchor(egui::Align2::RIGHT_BOTTOM, (-16.0, -16.0))
        .show(egui_context.ctx(),
              |ui| {
                  ui.checkbox(&mut spawning.simple, "Simple Ship AI (red)");
                  ui.checkbox(&mut spawning.smarter, "Smarter Ship AI (orange-red)");
                  ui.checkbox(&mut spawning.grid, "Grid Search AI (orange)");
                  ui.checkbox(&mut spawning.gradient, "Gradient Descent Ship AI (white)");
              });
}

fn orbit_system(time: Res<Time>, mut q: Query<(&Orbit, &mut Transform)>) {
    let t = time.seconds_since_startup() as f32;
    for (o, mut p) in q.iter_mut() {
        p.translation.x = o.radius * f32::cos(o.speed * t + o.true_anomaly);
        p.translation.y = o.radius * f32::sin(o.speed * t + o.true_anomaly);
    }
}

fn ship_name(kind: &str, id: usize) -> String {
    format!("{} {}", kind, id)
}

fn jp_spawn_system(mut commands: Commands,
                   spawning: Res<ShipTypeSpawns>,
                   mut ship_counts: ResMut<ShipSpawnCounter>,
                   planet: Res<ThePlanet>,
                   jps: Query<&Transform, With<JumpPoint>>) {
    for p in jps.iter() {
        if spawning.simple {
            commands.spawn()
                .insert(Name::new(ship_name("Simple AI", ship_counts.simple)))
                .insert(Fleet)
                .insert(Flight { target: planet.0, v: 80.0, dp: None, })
                .insert(SimpleReckoning)
                .insert_bundle(GeometryBuilder::build_as(
                    &shapes::Circle {
                        radius: 2.0,
                        center: Vec2::ZERO,
                    },
                    ShapeColors::new(Color::MAROON),
                    DrawMode::Fill(FillOptions::DEFAULT),
                    p.clone()
                ));
            ship_counts.simple += 1;
        }

        if spawning.smarter {
            commands.spawn()
                .insert(Name::new(ship_name("Smarter AI", ship_counts.smarter)))
                .insert(Fleet)
                .insert(Flight { target: planet.0, v: 80.0, dp: None, })
                .insert(SmarterReckoning)
                .insert_bundle(GeometryBuilder::build_as(
                    &shapes::Circle {
                        radius: 2.0,
                        center: Vec2::ZERO,
                    },
                    ShapeColors::new(Color::ORANGE_RED),
                    DrawMode::Fill(FillOptions::DEFAULT),
                    p.clone()
                ));
            ship_counts.smarter += 1;
        }

        if spawning.grid {
            commands.spawn()
                .insert(Name::new(ship_name("Grid Search AI", ship_counts.gradient)))
                .insert(Fleet)
                .insert(Flight { target: planet.0, v: 80.0, dp: None, })
                .insert(GridSearch)
                .insert_bundle(GeometryBuilder::build_as(
                    &shapes::Circle {
                        radius: 2.0,
                        center: Vec2::ZERO,
                    },
                    ShapeColors::new(Color::ORANGE),
                    DrawMode::Fill(FillOptions::DEFAULT),
                    p.clone()
                ));
            ship_counts.grid += 1;
        }

        if spawning.gradient {
            commands.spawn()
                .insert(Name::new(ship_name("Gradient Descent AI", ship_counts.gradient)))
                .insert(Fleet)
                .insert(Flight { target: planet.0, v: 80.0, dp: None, })
                .insert(GradientDescent)
                .insert_bundle(GeometryBuilder::build_as(
                    &shapes::Circle {
                        radius: 2.0,
                        center: Vec2::ZERO,
                    },
                    ShapeColors::new(Color::WHITE),
                    DrawMode::Fill(FillOptions::DEFAULT),
                    p.clone()
                ));
            ship_counts.gradient += 1;
        }
    }
}

fn spawn_dot(commands: &mut Commands, time: &Res<Time>, trails: &Res<Trails>, color: Color, p: Vec2) {
    if trails.enabled {
        commands.spawn()
            .insert(Name::new("dot"))
            .insert(Dot { creation: time.seconds_since_startup(), color })
            .insert_bundle(GeometryBuilder::build_as(
                &shapes::Circle {
                    radius: 1.0,
                    center: Vec2::ZERO,
                },
                ShapeColors::new(color),
                DrawMode::Fill(FillOptions::DEFAULT),
                Transform::from_translation((p, 0.0).into())
            ));
    }
}

fn dot_decay(mut commands: Commands,
             time: Res<Time>,
             trails: Res<Trails>,
             mut dots: Query<(Entity, &mut Dot)>) {
    let t = time.seconds_since_startup();
    for (e, mut dot) in dots.iter_mut() {
        let time_existing = t - dot.creation;
        if time_existing < trails.length {
            let frac = time_existing / trails.length;
            dot.color.set_a(frac as f32);
        } else {
            commands.entity(e).despawn();
        }
    }
}

//fn move_by_dp(mut commands: Commands,
//              mut ships: Query<(Entity, &mut Flight, &mut Transform), With<Fleet>>) {
//    for (e, mut f, mut p) in ships.iter_mut() {
//        if p.translation.distance(f.target) <= 1.0 {
//            commands.entity(e).despawn();
//        } else if let Some(dp) = f.dp {
//            p.translation += f.dp;
//        }
//    }
//}

#[derive(Debug)]
struct TargetMotion {
    position: Vec3,
    orbit: Option<Orbit>,
    flight: Option<Flight>,
}

fn ship_get_target(q: QuerySet<(Query<(&GlobalTransform, Option<&Orbit>, Option<&Flight>)>,
                                Query<&Flight, With<Fleet>>)>)
                   -> HashMap<Entity, TargetMotion> {
    let mut table: HashMap<Entity, TargetMotion> = Default::default();
    for f in q.q1().iter() {
        if let Ok((transform, orbit, flight)) = q.q0().get(f.target) {
            table.insert(f.target, TargetMotion {
                position: transform.translation,
                orbit: orbit.cloned(),
                flight: flight.cloned(),
            });
        }
    }
    table
}

fn simple_ship_system(In(motion): In<HashMap<Entity, TargetMotion>>,
                      mut commands: Commands,
                      time: Res<Time>,
                      trails: Res<Trails>,
                      mut ships: Query<(Entity, &mut Flight, &mut Transform), (With<Fleet>, With<SimpleReckoning>)>) {
    let t = time.delta_seconds();
    for (e, mut f, mut p) in ships.iter_mut() {
        match motion.get(&f.target) {
            Some(TargetMotion { position: destination,
                                orbit: _,
                                flight: _}) => {
                spawn_dot(&mut commands, &time, &trails, Color::MAROON * 0.2, p.translation.xy());
                if p.translation.distance(*destination) <= 1.0 {
                    commands.entity(e).despawn();
                    continue;
                }
                let Δp = *destination - p.translation;
                f.dp = Some(Δp.normalize() * f.v * t);
                p.translation += f.dp.unwrap();
            },
            Some(_) => return,
            None => return,
        };
    }
}

fn smarter_ship_system(In(motion): In<HashMap<Entity, TargetMotion>>,
                      mut commands: Commands,
                      time: Res<Time>,
                      trails: Res<Trails>,
                      mut ships: Query<(Entity, &mut Flight, &mut Transform), (With<Fleet>, With<SmarterReckoning>)>) {
    let t = time.delta_seconds();
    for (e, mut f, mut p) in ships.iter_mut() {
        match motion.get(&f.target) {
            Some(TargetMotion { position: destination,
                                orbit: Some(orbit),
                                flight: _}) => {
                spawn_dot(&mut commands, &time, &trails, Color::ORANGE_RED * 0.2, p.translation.xy());
                if p.translation.distance(*destination) <= 1.0 {
                    commands.entity(e).despawn();
                    continue;
                } else if p.translation.distance(*destination) <= 4.0 {
                    let Δp = *destination - p.translation;
                    f.dp = Some(Δp.normalize() * f.v * t);
                } else {
                    let current_time: f32 = time.seconds_since_startup() as f32;
                    let est_time = current_time + (p.translation.distance(*destination) / f.v);
                    let est_pos = Vec3::new(orbit.radius * f32::cos(orbit.speed * est_time + orbit.true_anomaly),
                                            orbit.radius * f32::sin(orbit.speed * est_time + orbit.true_anomaly),
                                            0.0);
                    let Δp = est_pos - p.translation;
                    f.dp = Some(Δp.normalize() * f.v * t);
                }
                p.translation += f.dp.unwrap();
            },
            Some(_) => return,
            None => return,
        };
    }
}

fn grid_ship_set_course(In(motion): In<HashMap<Entity, TargetMotion>>,
                        mut commands: Commands,
                        time: Res<Time>,
                        trails: Res<Trails>,
                        mut spawns: ResMut<ShipTypeSpawns>,
                        mut ships: Query<(Entity, &mut Flight, &mut Transform), (With<Fleet>, With<GridSearch>)>) {
    for (e, mut f, mut p) in ships.iter_mut() {
        if e != f.target {
            match motion.get(&f.target) {
                Some(TargetMotion { position,
                                    orbit: Some(orbit),
                                    flight: _}) => {
                    spawn_dot(&mut commands, &time, &trails, Color::ORANGE * 0.2, p.translation.xy());
                    let distance = p.translation.distance(*position);
                    if distance <= 1.0 || distance > 1000.0 {
                        commands.entity(e).despawn();
                    } else {
                        if f.dp.is_none() {
                            let ship_dist = p.translation.xy().distance(Vec2::ZERO);
                            let heading_to_center = if p.translation == Vec3::ZERO {
                                0.0
                            } else {
                                let center = Vec3::ZERO - p.translation;
                                f32::atan2(center.y, center.x)
                            };
                            let θ_range = if ship_dist <= orbit.radius {
                                std::f32::consts::TAU
                            } else {
                                2.0 * f32::asin(orbit.radius / ship_dist)
                            };
                            let θ_max = θ_range / 2.0;
                            let mut θ = -θ_max;
                            let θ_step = θ_range / 100.0;

                            let mut t_min = (ship_dist - orbit.radius) / f.v;
                            let t_max = (ship_dist + orbit.radius) / f.v;
                            let mut t = (ship_dist - orbit.radius) / f.v;
                            let t_step = (t_max - t_min) / 100.0;

                            let mut min_dist = f32::MAX;
                            let mut min_point = Vec2::ZERO;

                            let orbit_anomaly = f32::atan2(position.y, position.x);
                            while θ <= θ_max {
                                while t <= t_max {
                                    let planet_pos = Vec2::new(orbit.radius * f32::cos(orbit.speed * t + orbit_anomaly),
                                                               orbit.radius * f32::sin(orbit.speed * t + orbit_anomaly));
                                    let ship_pos = Vec2::new(p.translation.x + f.v * t * f32::cos(θ + heading_to_center),
                                                             p.translation.y + f.v * t * f32::sin(θ + heading_to_center));
                                    let dist = ship_pos.distance(planet_pos);
                                    if dist < min_dist {
                                        //spawn_dot(&mut commands, &time, &trails, Color::VIOLET, ship_pos);
                                        min_dist = dist;
                                        min_point = vec2(θ + heading_to_center, t);
                                        //dbg!(min_point, min_dist);
                                    } else {
                                        //spawn_dot(&mut commands, &time, &trails, Color::PURPLE, ship_pos);
                                    }
                                    t += t_step;
                                }
                                t = t_min;
                                θ += θ_step;
                            }
                            let heading = min_point[0];
                            println!("Grid Search: Found optimal heading {:?}, travel time {:?} (iterations {}, closest approach {})",
                                     heading,
                                     min_point[1],
                                     1000,
                                     min_dist);
                            f.dp = Some(Vec3::new(f32::cos(heading), f32::sin(heading), 0.0));
                            //spawns.grid = false;
                        }
                        if let Some(dp) = f.dp {
                            let t = time.delta_seconds();
                            p.translation += dp.normalize() * f.v * t;
                        }
                    }
                },
                Some(_) => return,
                None => return,
            };
        }
    }
}

fn gradient_ship_set_course(In(motion): In<HashMap<Entity, TargetMotion>>,
                            mut commands: Commands,
                            time: Res<Time>,
                            trails: Res<Trails>,
                            mut ships: Query<(Entity, &mut Flight, &mut Transform), (With<Fleet>, With<GradientDescent>)>) {
    for (e, mut f, mut p) in ships.iter_mut() {
        if e != f.target {
            match motion.get(&f.target) {
                Some(TargetMotion { position,
                                    orbit: Some(orbit),
                                    flight: _}) => {
                    spawn_dot(&mut commands, &time, &trails, Color::WHITE * 0.2, p.translation.xy());
                    fleet_match_orbit(&mut commands, &time, e, &mut f, &mut p, *position, orbit)
                },
                Some(_) => return,
                None => return,
            };
        }
    }
}

fn fleet_match_orbit(commands: &mut Commands, time: &Res<Time>, e: Entity, f: &mut Flight, p: &mut Transform, dest: Vec3, orbit: &Orbit) {
    let distance = p.translation.distance(dest);
    if distance <= 1.0 || distance > 1000.0 {
        commands.entity(e).despawn();
    } else {
        if f.dp.is_none() {
            let fleet_x = f64::from(p.translation.x);
            let fleet_y = f64::from(p.translation.y);
            let fleet_v = f64::from(f.v);
            let orbit_radius = f64::from(orbit.radius);
            let orbit_speed = f64::from(orbit.speed);
            let orbit_anomaly = f64::atan2(dest.y.into(), dest.x.into());
            use argmin::prelude::{ArgminOp, ArgminSlogLogger, ObserverMode, Error, Executor};
            use argmin::solver::gradientdescent::SteepestDescent;
            use argmin::solver::linesearch::MoreThuenteLineSearch;
            #[derive(Debug)]
            struct Func {
                fleet_x: f64,
                fleet_y: f64,
                fleet_v: f64,
                orbit_radius: f64,
                orbit_speed: f64,
                orbit_anomaly: f64,
            }
            impl ArgminOp for Func {
                type Param = Vec<f64>;
                type Output = f64;
                type Hessian = Vec<f64>;
                type Jacobian = Vec<f64>;
                type Float = f64;
                fn apply(&self, x: &Self::Param) -> Result<Self::Output, Error> {
                    let t = x[0];
                    let θ = x[1];
                    let planet_pos = DVec2::new(self.orbit_radius * f64::cos(self.orbit_speed * t + self.orbit_anomaly),
                                                self.orbit_radius * f64::sin(self.orbit_speed * t + self.orbit_anomaly));
                    let ship_pos = DVec2::new(self.fleet_x + self.fleet_v * t * f64::cos(θ),
                                              self.fleet_y + self.fleet_v * t * f64::sin(θ));
                    // this heavily penalizes solutions with negative time
                    let penalty = 10.0*f64::exp(-2.0*t);
                    Ok(ship_pos.distance(planet_pos) + penalty)
                }
                fn gradient(&self, x: &Self::Param) -> Result<Self::Param, Error> {
                    let t = x[0];
                    let θ = x[1];
                    let penalty = 10.0*f64::exp(-2.0*t);
                    Ok(vec!{
                        (2.0*(self.orbit_radius*f64::cos(t*self.orbit_speed+self.orbit_anomaly)-t*self.fleet_v*f64::cos(θ)-self.fleet_x+penalty)*((-self.orbit_radius*self.orbit_speed*f64::sin(t*self.orbit_speed+self.orbit_anomaly))-self.fleet_v*f64::cos(θ)-penalty)+2.0*(self.orbit_radius*self.orbit_speed*f64::cos(t*self.orbit_speed+self.orbit_anomaly)-self.fleet_v*f64::sin(θ)-penalty)*(self.orbit_radius*f64::sin(t*self.orbit_speed+self.orbit_anomaly)-t*self.fleet_v*f64::sin(θ)-self.fleet_y+penalty))/(2.0*f64::sqrt((self.orbit_radius*f64::sin(t*self.orbit_speed+self.orbit_anomaly)-t*self.fleet_v*f64::sin(θ)-self.fleet_y+penalty).powf(2.0)+(self.orbit_radius*f64::cos(t*self.orbit_speed+self.orbit_anomaly)-t*self.fleet_v*f64::cos(θ)-self.fleet_x+penalty).powf(2.0))),
                        (2.0*t*self.fleet_v*f64::sin(θ)*(self.orbit_radius*f64::cos(t*self.orbit_speed+self.orbit_anomaly)-t*self.fleet_v*f64::cos(θ)-self.fleet_x+penalty)-2.0*t*self.fleet_v*f64::cos(θ)*(self.orbit_radius*f64::sin(t*self.orbit_speed+self.orbit_anomaly)-t*self.fleet_v*f64::sin(θ)-self.fleet_y+penalty))/(2.0*f64::sqrt((self.orbit_radius*f64::sin(t*self.orbit_speed+self.orbit_anomaly)-t*self.fleet_v*f64::sin(θ)-self.fleet_y+penalty).powf(2.0)+(self.orbit_radius*f64::cos(t*self.orbit_speed+self.orbit_anomaly)-t*self.fleet_v*f64::cos(θ)-self.fleet_x+penalty).powf(2.0)))
                    })
                }
            }
            // start our search from a very naive guess about the starting conditions where we head straight for the target
            let est_time = p.translation.distance(dest) / f.v;
            let est_pos = Vec2::new(orbit.radius * f32::cos(orbit.speed * est_time + orbit.true_anomaly),
                                    orbit.radius * f32::sin(orbit.speed * est_time + orbit.true_anomaly));

            let init_param: Vec<f64> = vec![f64::from(p.translation.xy().distance(est_pos)) / f64::from(f.v),
                                            f64::from(Vec2::X.angle_between(est_pos.xy() - p.translation.xy()))];
            let solver = SteepestDescent::new(MoreThuenteLineSearch::new());
            let solution = Executor::new(Func { fleet_x, fleet_y, fleet_v, orbit_radius, orbit_speed, orbit_anomaly },
                                         solver, init_param)
                //.add_observer(ArgminSlogLogger::term(), ObserverMode::Always)
                .target_cost(0.5)
                .max_iters(2000)
                .run()
                .expect("solver failed");
            let heading = solution.state().best_param[1] as f32;
            println!("Gradient Descent: Found optimal heading {:?}, travel time {:?} (iterations {}, time spent {:?}, closest approach {})",
                     heading,
                     solution.state().best_param[0],
                     solution.state().iter,
                     solution.state().time.unwrap_or(std::time::Duration::ZERO),
                     solution.state().best_cost);
            f.dp = Some(Vec3::new(f32::cos(heading), f32::sin(heading), 0.0));
        }

        if let Some(dp) = f.dp {
            let t = time.delta_seconds();
            p.translation += dp.normalize() * f.v * t;
        }
    }
}
